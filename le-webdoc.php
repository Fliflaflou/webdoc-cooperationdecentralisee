<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Coopération décentralisée France-Sénégal - Niveau 2</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>

    <body id="niveau2" onload="showPage()">
        <div id="loader"></div>
        <div id="webdocContenu" style="display:none;" >
	<?php include("./header-webdoc.php"); ?>  <!-- intègre le menu -->
	
	<div id="logo" title="Survolez l'image pour accéder aux thématiques">
	    <img class="logo" id="pt-rouage-niv2" src="img/petit-rouage-niveau2.png" alt="rouage animé" />
	    <img class="logo" id="gd-rouage-niv2" src="img/gd-rouage-niveau2.png" alt="rouage animé" />
	</div>

	<div class="svg-container">
            <svg version="1.1" viewbox="0 0 3000 2000" preserveAspectRatio="none" class="svg-content">
            <g>
	    <title>Coopérer, qu'est-ce que l'on en retient ?</title>
            <a id="rayon-1" xlink:href="01-cooperer-qu-est-ce-que-l-on-en-retient.php" onmouseover="afficheTitre1()" onmouseout="effaceTitre()">
		<polygon class="rayon-svg" points="1500,1000 3000,0 3000,1000" />
	    </a>
	    </g>
	    <g>
	    <title>Qu'est-ce qui nous anime ?</title>
	    <a id="rayon-2" xlink:href="02-ce-qui-nous-anime.php" onmouseover="afficheTitre2()" onmouseout="effaceTitre()">
                <polygon class="rayon-svg" points="1500,1000 3000,1000 3000,2000" />
	    </a>
	    </g>
	    <g>
	    <title>Quand il faut décider</title>
	    <a id="rayon-3" xlink:href="03-quand-il-faut-decider.php" onmouseover="afficheTitre3()" onmouseout="effaceTitre()">
		<polygon class="rayon-svg" points="1500,1000 3000,2000 1500,2000" />
	    </a>
	    </g>
	    <g>
	    <title>À quoi ça sert ?</title>
            <a id="rayon-4" xlink:href="04-a-quoi-ca-sert.php" onmouseover="afficheTitre4()" onmouseout="effaceTitre()">
		<polygon class="rayon-svg" points="1500,1000 1500,2000 0,2000" />
	    </a>
	    </g>
	    <g>
	    <title>Quels changements ?</title>
	    <a id="rayon-5" xlink:href="05-quels-changements.php" onmouseover="afficheTitre5()" onmouseout="effaceTitre()">
		<polygon class="rayon-svg" points="1500,1000 0,2000 0,1000" />
	    </a>
	    </g>
	    <g>
	    <title>Des différences constructives</title>
	    <a id="rayon-6" xlink:href="06-des-differences-constructives.php" onmouseover="afficheTitre6()" onmouseout="effaceTitre()">
		<polygon class="rayon-svg" points="1500,1000 0,1000 0,0" />
	    </a>
	    </g>
	    <g>
	    <title>Mais encore...</title>
	    <a id="rayon-7" xlink:href="07-mais-encore.php" onmouseover="afficheTitre7()" onmouseout="effaceTitre()">
		<polygon class="rayon-svg" points="1500,1000 0,0 1500,0" />
	    </a>
            </g>
            </svg>
        </div>
	<div id="titre-1" class="entree-niveau3" onmouseover="survolTitre1()"><a class="hover-titre" href="01-cooperer-qu-est-ce-que-l-on-en-retient.php">Coopérer, qu'est-ce que l'on en retient&nbsp;?</a></div> 
	<div id="titre-2" class="entree-niveau3" onmouseover="survolTitre2()"><a class="hover-titre" href="02-ce-qui-nous-anime.php">Qu'est-ce qui nous anime&nbsp;?</a></div>
	<div id="titre-3" class="entree-niveau3" onmouseover="survolTitre3()"><a class="hover-titre" href="03-quand-il-faut-decider.php">Quand il faut décider</a></div>
	<div id="titre-4" class="entree-niveau3" onmouseover="survolTitre4()"><a class="hover-titre" href="04-a-quoi-ca-sert.php">À quoi ça sert&nbsp;?</a></div>
	<div id="titre-5" class="entree-niveau3" onmouseover="survolTitre5()"><a class="hover-titre" href="05-quels-changements.php">Quels changements&nbsp;?</a></div>
	<div id="titre-6" class="entree-niveau3" onmouseover="survolTitre6()"><a class="hover-titre" href="06-des-differences-constructives.php">Des différences constructives</a></div>
	<div id="titre-7" class="entree-niveau3" onmouseover="survolTitre7()"><a class="hover-titre" href="07-mais-encore.php">Mais encore...</a></div>
        <footer>
	    <audio id="ambiance" autoplay muted loop>
		<source src="audio/a-quoi-ca-sert.mp3" type="audio/mpeg">
		<source src="audio/a-quoi-ca-sert.ogg" type="audio/ogg">
                Your browser does not support the audio element.
            </audio>
            <?php include("./equalizer.php"); ?>  <!-- intègre l'equalizer -->
	    <?php include("./credits.php"); ?>  <!-- intègre les crédits -->
        </footer>
        </div>
	<script src="js/script.js"></script>
        <script src="js/script-niveau2.js"></script>
    </body>
</html>
