<header>
    <h2 id="menu" class="text-center">Menu</h2>
    <div id="rouage-menu"><img src="img/rouage-menu.png" alt="rond" /></div>
    <div id="sous-menu">
        <div id="triangle"></div>
        <ul class="text-center" id="entrees-menu">
            <li><a href="le-sujet.php" class="inactive">Quel est le sujet&nbsp;?</a></li>
            <li><a href="les-acteurs.php" class="inactive">Qui sont les acteurs&nbsp;?</a></li>
            <li><a href="le-webdoc.php" class="menu-actif">Le webdoc</a></li>
        </ul>
    </div>
</header>