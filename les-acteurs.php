<!DOCTYPE html>
<html lang="fr">
    <head>
	<title>Coopération décentralisée France-Sénégal - Les acteurs</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>

    <body id="acteurs">
	<header>
	    <div id="rouage-menu"><img src="img/rouage-menu.png" alt="rond" /></div>
	    <div id="sous-menu">
		<div id="triangle"></div>
		<ul class="text-center" id="entrees-menu">
		    <li><a href="le-sujet.php" class="inactive">Quel est le sujet&nbsp;?</a></li>
		    <li><a href="les-acteurs.php" class="menu-actif">Qui sont les acteurs&nbsp;?</a></li>
                    <li><a href="le-webdoc.php" class="inactive">Le webdoc</a></li>
		</ul>
	    </div>
	</header>
	<div class="container-fluid">
	    <div class="row">
		<div class="col-sm-6 col-gauche">
		    <p><strong>Dans le rôle des maitres d’ouvrage&nbsp;:</strong></p>
		    <ul>
			<li><a href="http://www.ardeche.fr/" target="_blank">Le Département de l’Ardèche</a> et <a href="https://www.ladrome.fr/le-departement" target="_blank">le Département de la Drôme</a></li>
			<li>Les Communes d’Agnam Civol, Orkadiere et Wouro Sidy</li>
		    </ul>
		    <p><strong>L'’artisan pour la mise en lien et la mise en œuvre&nbsp;:</strong></p>
		    <ul>
			<li><a href="http://www.ados-association.org/" target="_blank">L’association ADOS</a></li>
		    </ul>
		    <p><strong>Expertise et appui financier&nbsp;:</strong></p>
		    <ul>
			<li><a href="https://www.eaurmc.fr/" target="_blank">Agence de l’Eau Rhône Méditerranée Corse</a></li>
			<li><a href="https://www.diplomatie.gouv.fr/fr/" target="_blank">Ministère de l’Europe et des affaires étrangères (MEAE)</a></li>
			<li><a href="https://www.auvergnerhonealpes.fr/" target="_blank">Région Auvergne Rhône Alpes</a></li>
			<li><a href="http://www.mha.gouv.sn/" target="_blank">Ministère sénégalais de l’Hydraulique et de l’Assainissement</a></li>
			<li><a href="http://www.forages-ruraux.sn/" target="_blank">Office des Forages Ruraux</a></li>
			<li><a href="http://decentralisation.gouv.sn/" target="_blank">Ministère sénégalais de la Gouvernance Locale</a></li>
			<li><a href="https://www.agenceecofin.com/" target="_blank">Agence Nationale des Eco Villages</a></li>
		    </ul>
		    <p><strong>Appui technique et transfert de compétences&nbsp;:</strong></p>
		    <ul>
			<li>Services techniques des Départements de l’Ardèche et de la Drôme</li>
			<li>Agence Régionale de Développement de Matam</li>
			<li>Division Régionale de l’Hydraulique, région de Matam</li>
			<li>Brigade des Puits et Forages, région de Matam</li>
			<li>Service Régional de l’Assainissement, région de Matam</li>
			<li><a href="http://www.impotsetdomaines.gouv.sn/" target="_blank">Service des Impôts</a>, région de Matam</li>
			<li>Association régionale des Conducteurs de Forage de Matam</li>
			<li><a href="http://club.quomodo.com/cfpo-sn/" target="_blank">Centre de Formation Professionnelle de Ourossogui</a>, région de Matam</li>
			<li><a href="http://www.btpcfa-rhonealpes.fr/nos-cfa/btp-cfa-drome-ardeche/" target="_blank">Centre de Formation des Apprentis Batipôle</a>, département de la Drôme</li>
			<li><a href="http://leon-pavin.elycee.rhonealpes.fr/" target="_blank">Lycée professionnel Léon Pavin, Chomérac</a>, département de l’Ardèche</li>
			<li>Coordination Rhône Alpes en région de Matam</li>
			<li>Cellule de Coordination du PEPAM, Dakar</li>
			<li><a href="http://www.dgpre.gouv.sn/" target="_blank">Direction de la Gestion et de la Planification des Ressources en Eau</a>, Dakar</li>
			<li><a href="http://www.mfpaa.gouv.sn/" target="_blank">Ministère de la Formation Professionnelle, de l’Apprentissage et de l’Artisanat</a>, Dakar</li>
			<li><a href="https://sn.ambafrance.org/Le-Service-de-Cooperation-et-d-Action-Culturelle" target="_blank">Ambassade de France et Service de la Coopération de l’Action Culturelle</a></li>
		    </ul>
		</div>
		<div class="col-sm-6 col-droite">
		    <p><strong>Réseaux d’appui&nbsp;:</strong></p>
		    <ul>
			<li><a href="http://www.resacoop.org" target="_blank">GIP RESACOOP</a></li>
			<li><a href="https://www.pseau.org/" target="_blank">Programme Solidarité Eau</a></li>
			<li><a href="http://www.ciedel.org" target="_blank">Centre International d’Etudes du Développement Local</a></li>
		    </ul>
		    <p><strong>Et remerciements spéciaux à</strong></p>
		    <ul>
			<li>Gouvernance de la région de Matam</li>
			<li>Préfectures et sous-préfectures du département de Matam, Kanel et Ranérou</li>
			<li>Conseils départementaux de Matam, Kanel et Ranérou</li>
			<li><a href="http://www.valence.fr" target="_blank">Ville de Valence</a> et <a href="http://valenceromansagglo.fr" target="_blank">Valence Romans Agglo</a></li>
			<li>Les groupements féminins et organisations communautaires de la région de Matam</li>
			<li>Les collèges, lycées, associations de jeunes de l’Ardèche, de la Drôme et de Matam</li>
			<li>L’Inspection d’Académie de Matam et l’<a href="http://www.ac-grenoble.fr/" target="_blank">Académie du Rectorat de Grenoble</a></li>
			<li>Radio Salndu Fouta</li>
			<li><a href="https://www.france-volontaires.org/" target="_blank">France Volontaires</a></li>
			<li><a href="http://adl.sn/acteur/ams" target="_blank">Association des Maires du Sénégal</a></li>
			<li><a href="http://www.departements.fr/" target="_blank">Association des Départements de France</a></li>
			<li>Association des Départements du Sénégal</li>
			<li><a href="http://formations.univ-grenoble-alpes.fr/fr/catalogue/licence-professionnelle-DP/droit-economie-gestion-DEG/licence-professionnelle-metiers-de-la-protection-et-de-la-gestion-de-l-environnement-program-licence-professionnelle-metiers-de-la-protection-et-de-la-gestion-de-l-environnement/parcours-economie-et-gestion-de-l-eau-et-des-ressources-valence-subprogram-parcours-economie-et-gestion-de-l-eau-et-des-ressources.html" target="_blank">Université Grenoble Alpes, Faculté d’économie, Licence gestion durable de l’eau</a></li>
			<li>Bertrand Desmares et à l’ensemble des personnes ressources mobilisées tout au long du projet !</li>
		    </ul>
		</div>
	    </div>
	</div>
	<footer>
            <?php include("./retour-webdoc.php"); ?>  <!-- intègre la flèche retour au webdoc -->
	    <?php include("./credits.php"); ?>  <!-- intègre les crédits -->
	</footer>
    </body>
</html>
