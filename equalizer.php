<div id="equalizer" onclick="enableMute()" title="activer / désactiver le son">
    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs></defs>
        <g id="icon-equalizer-anim" fill="#4A4A4A">
            <rect class="barre" id="eq1" x="1" y="8" width="4" height="8"></rect>
            <rect class="barre" id="eq2" x="6" y="1" width="4" height="15"></rect>
            <rect class="barre" id="eq3" x="11" y="4" width="4" height="12"></rect>
        </g>
    </svg>
    <div class="tip-son">Pour activer le son, c'est ici !</div>
</div>