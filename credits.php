<div class="lien-credits">
    <a href="#" data-toggle="modal" data-target="#credits">Crédits</a>
</div>
<div class="modal fade" tabindex="-1" id="credits" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <h2>Crédits</h2>
                <ul>
                    <li>Réalisation, vidéos, sons, textes et photos (sauf mention)&nbsp;: <a href="http://rezonance.media" target="_blank">réZonance</a></li>
                    <li>Graphisme&nbsp;: <a href="http://www.levideographe.com" target="_blank">le Vidéographe</a></li>
                    <li>Intégration et développement&nbsp;: <a href="https://3pix.fr" target="_blank">3PIX</a></li>
                    <li>Responsables du projet&nbsp;: <a href="http://www.ardeche.fr/" target="_blank">Département de l’Ardèche</a>, <a href="https://www.ladrome.fr/le-departement" target="_blank">Département de la Drôme</a>, <a href="http://www.ados-association.org/" target="_blank">ADOS</a></li>
                </ul>
                <h2>Remerciements</h2>
                <p>Souleye BA, Adja LAM, Abdoulaye SY, Abou FALL, Mass DIALLO, Djiby DIOL, Mouhamadou SYLLA, Aichatou DIENE, Aminata BA, Halim Fall, Lansana SAKHO, Mamadou AMARIKA, Ndiogou FALL, Penda DIA, Youssoupha BA, Ousmane DIOUF, Samba CAMARA, Amidou DIALLO, Ouleye DIALLO, Amadou DIALLO, Julie CAMY, Denis DUCHAMP, Christine BENOIT, Sylvie DUBOIS, Isabelle SEREN, Alain PERRIER, Pierre CHATTE, Alain BABYLON, François DOLS, Patricia BRUNEL MAILLET, Rémi TOURON, Violaine DIDIER, Michaële GROSHANS, Alanne BONNARD, Hélène MILLET, Didier SERRE, Laurence ROCHER, Bertrand DESMARES</p>
                <h2>Mentions légales</h2>
                <h3>Édition du site Internet</h3>
                <p>Le site SITE (ci-après "le Site"), accessible à l’adresse URL est édité par ADOS (ci-après "l'Editeur"), Association Loi 1901, dont le siège social est situé Allée du Concept, Quartier Girodet, 26500 Bourg-lès-Valence (+33 (0)4.75.55.99.90, contact@ados-association.org).<br>
                    RNA : 
                <ul>
                    <li>Directrice de la publication&nbsp;: Rachel NODIN, Présidente d'ADOS</li>
                    <li>Contact&nbsp;: +33 (0)4.75.55.99.90 / contact@ados-association.org</li>
                    <li>Hébergeur&nbsp;: le Site est hébergé par HEBERGEUR, FORME SOCIALE au capital de FORME SOCIALE euros, dont le siège social est situé ADRESSE HEBERGEUR, joignable par le moyen suivant : TELEPHONE.</li>
                </ul>
                <h3>Respect de la propriété intellectuelle</h3>
                <p>Toutes les marques, photographies, textes, commentaires, illustrations, images animées ou non, séquences vidéo, sons, ainsi que toutes les applications informatiques qui pourraient être utilisées pour faire fonctionner le Site et plus généralement tous les éléments reproduits ou utilisés sur le Site sont protégés par les lois en vigueur au titre de la propriété intellectuelle.
                    Ils sont la propriété pleine et entière de l'Editeur ou de ses partenaires, sauf mentions particulières. Toute reproduction, représentation, utilisation ou adaptation, sous quelque forme que ce soit, de tout ou partie de ces éléments, y compris les applications informatiques, sans l'accord préalable et écrit de l'Editeur, sont strictement interdites. Le fait pour l'Editeur de ne pas engager de procédure dès la prise de connaissance de ces utilisations non autorisées ne vaut pas acceptation desdites utilisations et renonciation aux poursuites.
                    Seule l'utilisation pour un usage privé dans un cercle de famille est autorisée et toute autre utilisation est constitutive de contrefaçon et/ou d'atteinte aux droits voisins, sanctionnées par Code de la propriété intellectuelle.
                    La reprise de tout ou partie de ce contenu nécessite l'autorisation préalable de l'Editeur ou du titulaire des droits sur ce contenu.</p>
                <h3>Liens hypertextes</h3>
                <p>Le Site peut contenir des liens hypertexte donnant accès à d'autres sites web édités et gérés par des tiers et non par l'Editeur. L'Editeur ne pourra être tenu responsable directement ou indirectement dans le cas où lesdits sites tiers ne respecteraient pas les dispositions légales.</p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" title="Fermer" class="ferme-video"><img src="img/fleche-retour.png" alt="flèche retour"></a>
            </div>
        </div>
    </div>
</div>