<!DOCTYPE html>
<html lang="fr">
    <head>
	<title>Coopération décentralisée France-Sénégal - Le sujet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>

    <body id="sujet">
	<header>
	    <!-- <h2 id="menu" class="text-center">Menu</h2> -->
	    <div id="rouage-menu"><img src="img/rouage-menu.png" alt="rond" /></div>
	    <div id="sous-menu">
		<div id="triangle"></div>
		<ul class="text-center" id="entrees-menu">
		    <li><a href="le-sujet.php" class="menu-actif">Quel est le sujet&nbsp;?</a></li>
		    <li><a href="les-acteurs.php" class="inactive">Qui sont les acteurs&nbsp;?</a></li>
		    <li><a href="le-webdoc.php" class="inactive">Le webdoc</a></li>
		</ul>
	    </div>
	</header>
	<div class="container-fluid">
	    <div class="row">
		<div class="col-md-3">
		    <ul>
			<li>Un programme sur 6 ans autour de l’accès à l’eau.</li>
			<li>Une coopération entre les Départements de l’Ardèche et de La Drôme (région Auvergne Rhône Alpes, France) et les collectivités locales de la région de Matam (Nord Est du Sénégal).</li>
			<li>14 collectivités sénégalaises renforcées dans leur autonomie et leurs compétences.</li>
			<li>5&nbsp;650&nbsp;000 euros investis sur 6 ans (collectivités locales françaises et sénégalaises, Agence de l’eau, les Etat français et sénégalais, les usagers des services de l’eau).</li>
			<li>7 Adductions d’Eau Potable réalisées, dont 5 forages entre 230 et 500 m de profondeur.</li>
			<li>90 km de réseaux construits, desservant 41 villages.</li>
			<li>23&nbsp;600 habitants desservis en eau potable.</li>
			<li>30 abreuvoirs permettant l’abreuvement de plus de 45&nbsp;600  têtes de bétail.</li>
			<li>45 bornes fontaines et 4 potences.</li>
			<li>83 robinets privés.</li>
			<li>25 latrines privées et 5 latrines publiques.</li>
			<li>15 artisans et 80 conducteurs de forage formés.</li>
			<li>33 associations d’usagers accompagnés dans leur gestion du service de l’eau.</li>
			<li>Une filière de formation canalisation plomberie (CAP) créée et 65 jeunes formés (2013-2017).</li>
		    </ul>
		</div>
		<div class="col-md-3">
		    <ul>
			<li>13 missions de suivi organisées au Sénégal, 6 missions en France.</li>
			<li>27 rencontres et ateliers organisés au Sénégal et en France entre 2010 et 2017.</li>
			<li>1 état des lieux précis de 233 forages composant le parc régional de Matam.</li>
			<li>1 évaluation de la disponibilité et de la qualité de la ressource en eau en région de Matam.</li>
			<li>Plus de 30 professionnels français et sénégalais mobilisés pour apporter leur expertise.</li>
			<li>8 chantiers-école organisés au Sénégal.</li>
			<li>Plus de 1&nbsp;640 jeunes français et sénégalais impliqués dans des échanges interculturels autour du thème de l’eau.</li>
			<li>1 opérateur commun chargé de la mise en œuvre : l’association ADOS basée en France et au Sénégal.</li>
			<li>Une expérience considérée comme pilote par l’État du Sénégal dans la perspective de la réforme de l’hydraulique rurale.</li>
		    </ul>
		    <p>Nous avons souhaité interroger cette expérience de coopération décentralisée en donnant la parole à différents élus locaux, techniciens, usagers, ayant participé au programme.</p>
		</div>
		<div class="col-md-6">
		    <h2>Programme hydraulique - Région de Matam Sénégal</h2>
		    <p style="clear: both; text-align: right;">Une vidéo de Bertrand Desmares</p>
		    <iframe src="https://player.vimeo.com/video/341501960" width="960" height="540" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
		    
		</div>
	    </div>
	</div>
	<footer>
            <?php include("./retour-webdoc.php"); ?>  <!-- intègre la flèche retour au webdoc -->
	    <?php include("./credits.php"); ?>  <!-- intègre les crédits -->	    
        </footer>	
    </body>
</html>
