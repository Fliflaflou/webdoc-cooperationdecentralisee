<!DOCTYPE html>
<html lang="fr">
    <head>
	<title>Coopération décentralisée France-Sénégal - Introduction</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body id="niveau1" onload="showPage()" >
	<div id="loader"></div>
	<div id="leContenu" style="display:none;" >
	    <div class="vimeo-wrapper">
		<iframe id="video-niveau1" src="https://player.vimeo.com/video/322818917?background=1&muted=1&autoplay=1&loop=1&byline=0&title=0" allowfullscreen allow=autoplay></iframe>
	    </div>
	    <div class="container-fluid">
		<div id="logo">
		    <img class="logo" src="img/logo-c.png" alt="Coopération décentralisée Ardèche Drôme Kanel Matam" />
		    <img class="logo" id="pt-rouage" src="img/logo-petit-rouage.png" alt="rouage animé" />
		    <img class="logo" id="gd-rouage" src="img/logo-grand-rouage.png" alt="rouage animé" />
		    <img class="logo" src="img/logo-coop-decentralisee.png" alt="Coopération décentralisée Ardèche Drôme Kanel Matam" />
		</div>
		<div class="container">
		    <div class="row parent-defile">
			<div class="texte defile">
			    <p>Un programme sur 6 ans autour de l’accès à l’eau.<br>
				Une coopération entre les Départements de l’Ardèche et de La Drôme (région Auvergne Rhône Alpes, France) et les collectivités locales de la région de Matam (Nord Est du Sénégal).</p>
			    <ul>
				<li>Programme hydraulique et assainissement régional (PHAR) 2010-2016 en région de Matam Sénégal.</li>
				<li>5 650 000 euros investis.</li>
				<li>7 forages et 90 km de réseaux construits.</li>
				<li>41 villages et 23 000 habitants desservis en eau potable.</li>
				<li>65 jeunes formés en canalisation plomberie.</li>
				<li>25 acteurs racontent cette coopération autour des enjeux de l’accès à l’eau entre 14 collectivités sénégalaises et 2 collectivités françaises.</li>
			    </ul>
			</div>
		    </div>
		    <div id="up"><img src="img/fleche-haut.png" alt="flèche haut" /></div>
		    <div id="down"><img src="img/fleche-bas.png" alt="flèche bas" /></div>
		    <div class="row" id="entrer">
                        <a href="le-webdoc.php"><img src="img/picto-entrer-texte.png" alt="rouage entrer" /></a>
		    </div>
		</div>
	    </div>
	    <footer>
		<div id="equalizer" onclick="unmuteVideo()">
		    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		    <defs></defs>
		    <g id="icon-equalizer-anim" fill="#4A4A4A">
		    <rect class="barre" id="eq1" x="1" y="8" width="4" height="8"></rect>
		    <rect class="barre" id="eq2" x="6" y="1" width="4" height="15"></rect>
		    <rect class="barre" id="eq3" x="11" y="4" width="4" height="12"></rect>
		    </g>
		    </svg>
		    <div class="tip-son">Pour activer le son, c'est ici !</div>
		</div>
		<?php include("./credits.php"); ?>  <!-- intègre les crédits -->
	    </footer>
	</div>	
	<script src="https://player.vimeo.com/api/player.js"></script>
        <script src="js/script-niveau1.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>
