<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Coopération décentralisée France-Sénégal - À quoi ça sert ?</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body id="niveau3-svg" onload="showPage()">
        <div id="loader"></div>
        <?php include("./header-webdoc.php"); ?>  <!-- intègre le menu -->
        <div>
            <h1 id="titre-sert">À quoi ça sert&nbsp;?</h1>
        </div>
        <div id="leContenu" style="display:none;" >
            <div id="rouage-centre">
                <img id="img-rouage-centre" src="img/rouage-centre.png" alt="rouage central" class="rotation" title="cliquez sur une image pour ouvrir une vidéo">
            </div>

            <!-- Portraits - DEBUT -->
            <div id="bloc-svg">
                <svg
                    viewBox="0 0 187.55829 138.63685"
                    class="niveau3-svg-content"
                    id="aQuoiCaSert">
                <g transform="translate(-80.080267,-106.28907)">
                <a class="lien-portrait" data-nom="Aminata Ba, Secrétaire générale de l’ASUFOR de Gassamberi" data-fond="aminata-ba" data-toggle="modal" data-target="#modal-322238033" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Aminata Ba</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 150.60838,148.48556 -6.19587,2.42341 2.01305,5.74346 a 19.308852,19.308852 0 0 0 -6.30782,5.68438 l -5.50802,-2.61724 -3.06795,5.90335 5.70523,2.71053 a 19.308852,19.308852 0 0 0 -0.70029,5.64007 19.308852,19.308852 0 0 0 0.27327,2.66465 l -5.97663,2.09434 2.42341,6.19639 5.74398,-2.01307 a 19.308852,19.308852 0 0 0 5.68387,6.30783 l -2.61724,5.50803 5.90334,3.06795 2.71053,-5.70523 a 19.308852,19.308852 0 0 0 5.64008,0.70029 19.308852,19.308852 0 0 0 2.66465,-0.27327 l 2.09486,5.97661 6.19586,-2.4234 -2.01306,-5.74398 a 19.308852,19.308852 0 0 0 6.30783,-5.68386 l 5.50802,2.61724 3.06794,-5.90386 -5.70523,-2.71053 a 19.308852,19.308852 0 0 0 0.7003,-5.63956 19.308852,19.308852 0 0 0 -0.27327,-2.66465 l 5.97662,-2.09486 -2.4234,-6.19587 -5.74347,2.01305 a 19.308852,19.308852 0 0 0 -5.68437,-6.30781 l 2.61723,-5.50803 -5.90334,-3.06795 -2.71053,5.70523 a 19.308852,19.308852 0 0 0 -5.64007,-0.70029 19.308852,19.308852 0 0 0 -2.66516,0.2738 z" id="vid-322238033" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Pierre Chatte, Chef d'entreprise à la retraite" data-fond="pierre-chatte" data-toggle="modal" data-target="#modal-322269832" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Pierre Chatte</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 117.37185,156.6188 -4.05229,0.41253 0.24473,3.71809 a 11.821738,11.821738 0 0 0 -4.65351,2.31759 l -2.81929,-2.44773 -2.77846,2.97848 2.92029,2.53513 a 11.821738,11.821738 0 0 0 -1.33872,3.21179 11.821738,11.821738 0 0 0 -0.27615,1.61656 l -3.86897,0.25441 0.41245,4.05259 3.7184,-0.24466 a 11.821738,11.821738 0 0 0 2.31729,4.65345 l -2.44774,2.81927 2.97848,2.77847 2.53513,-2.92029 a 11.821738,11.821738 0 0 0 3.21179,1.33872 11.821738,11.821738 0 0 0 1.61655,0.27615 l 0.25472,3.86904 4.05229,-0.41252 -0.24466,-3.7184 a 11.821738,11.821738 0 0 0 4.65344,-2.31729 l 2.81928,2.44774 2.77855,-2.97879 -2.92029,-2.53513 a 11.821738,11.821738 0 0 0 1.33864,-3.21148 11.821738,11.821738 0 0 0 0.27615,-1.61656 l 3.86905,-0.25471 -0.41253,-4.05229 -3.71809,0.24474 a 11.821738,11.821738 0 0 0 -2.31759,-4.65353 l 2.44774,-2.81927 -2.97849,-2.77847 -2.53513,2.92029 a 11.821738,11.821738 0 0 0 -3.21179,-1.33873 11.821738,11.821738 0 0 0 -1.61694,-0.27592 z" id="vid-322269832" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Michaële Groshans, ancienne Directrice ADOS (2013 à 2017)" data-fond="michaele-groshans" data-toggle="modal" data-target="#modal-322252061" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Michaële Groshans</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 251.89095,128.36089 -4.03419,-0.56258 -0.64612,3.66969 a 11.821738,11.821738 0 0 0 -5.07105,1.14495 l -2.15661,-3.04775 -3.40685,2.23262 2.23395,3.15665 a 11.821738,11.821738 0 0 0 -2.06384,2.80149 11.821738,11.821738 0 0 0 -0.65251,1.50458 l -3.81854,-0.67259 -0.56273,4.03447 3.66997,0.64627 a 11.821738,11.821738 0 0 0 1.14467,5.07091 l -3.04776,2.1566 2.23263,3.40685 3.15665,-2.23394 a 11.821738,11.821738 0 0 0 2.80149,2.06383 11.821738,11.821738 0 0 0 1.50457,0.65251 l -0.6723,3.81869 4.03419,0.56258 0.64627,-3.66996 a 11.821738,11.821738 0 0 0 5.0709,-1.14468 l 2.1566,3.04776 3.407,-2.23291 -2.23394,-3.15665 a 11.821738,11.821738 0 0 0 2.06369,-2.80121 11.821738,11.821738 0 0 0 0.65251,-1.50457 l 3.81869,0.67231 0.56258,-4.03419 -3.66969,-0.64613 a 11.821738,11.821738 0 0 0 -1.14495,-5.07105 l 3.04776,-2.1566 -2.23263,-3.40686 -3.15666,2.23395 a 11.821738,11.821738 0 0 0 -2.80148,-2.06384 11.821738,11.821738 0 0 0 -1.505,-0.65238 z" id="vid-322252061" class="chemin"/></a>
                <a class="lien-portrait" data-nom="Alain Babylon, ancien Chef du Service Gestion de l’Eau du Département de la Drôme" data-fond="alain-babylon" data-toggle="modal" data-target="#modal-322235608" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Alain Babylon</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 226.00863,139.63137 -4.01889,-0.66309 -0.73751,3.65242 a 11.821738,11.821738 0 0 0 -5.09805,1.01804 l -2.07987,-3.10064 -3.46151,2.14691 2.15447,3.21142 a 11.821738,11.821738 0 0 0 -2.13311,2.74911 11.821738,11.821738 0 0 0 -0.68986,1.48782 l -3.80056,-0.76768 -0.66325,4.01917 3.6527,0.73766 a 11.821738,11.821738 0 0 0 1.01776,5.0979 l -3.10064,2.07986 2.14691,3.46151 3.21142,-2.15446 a 11.821738,11.821738 0 0 0 2.74911,2.13311 11.821738,11.821738 0 0 0 1.48782,0.68985 l -0.7674,3.80072 4.01889,0.66309 0.73766,-3.65269 a 11.821738,11.821738 0 0 0 5.0979,-1.01776 l 2.07986,3.10063 3.46166,-2.14717 -2.15446,-3.21143 a 11.821738,11.821738 0 0 0 2.13296,-2.74883 11.821738,11.821738 0 0 0 0.68986,-1.48782 l 3.80072,0.76741 0.66309,-4.0189 -3.65242,-0.73751 a 11.821738,11.821738 0 0 0 -1.01804,-5.09805 l 3.10064,-2.07986 -2.14691,-3.46152 -3.21143,2.15447 a 11.821738,11.821738 0 0 0 -2.7491,-2.13311 11.821738,11.821738 0 0 0 -1.48825,-0.68974 z" id="vid-322235608" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Mamadou Hamady Ka, Chef du village de Bagondé" data-fond="mamadou-hamady" data-toggle="modal" data-target="#modal-322241550" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Mamadou Hamady Ka</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 138.67826,193.73122 -6.42703,1.71897 1.36149,5.93178 a 19.308852,19.308852 0 0 0 -6.90113,4.94724 l -5.18261,-3.21384 -3.70574,5.52532 5.36821,3.3285 a 19.308852,19.308852 0 0 0 -1.32349,5.52714 19.308852,19.308852 0 0 0 -0.0249,2.6785 l -6.17254,1.41635 1.71892,6.42755 5.93229,-1.36146 a 19.308852,19.308852 0 0 0 4.94673,6.90108 l -3.21385,5.18262 5.52533,3.70574 3.3285,-5.36821 a 19.308852,19.308852 0 0 0 5.52713,1.32349 19.308852,19.308852 0 0 0 2.67851,0.0249 l 1.41686,6.17259 6.42703,-1.71896 -1.36145,-5.9323 a 19.308852,19.308852 0 0 0 6.90109,-4.94672 l 5.18261,3.21384 3.70579,-5.52584 -5.36822,-3.3285 a 19.308852,19.308852 0 0 0 1.32345,-5.52662 19.308852,19.308852 0 0 0 0.0249,-2.67851 l 6.1726,-1.41685 -1.71897,-6.42705 -5.93178,1.3615 a 19.308852,19.308852 0 0 0 -4.94724,-6.90112 l 3.21384,-5.18261 -5.52532,-3.70575 -3.3285,5.36821 a 19.308852,19.308852 0 0 0 -5.52714,-1.32348 19.308852,19.308852 0 0 0 -2.67907,-0.0245 z" id="vid-322241550" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Christine Benoit, Directrice adjointe de la direction des territoires, Département de l’Ardèche" data-fond="christine-benoit" data-toggle="modal" data-target="#modal-322239635" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Christine Benoit</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 98.904284,183.2804 -5.1229,1.31932 1.03875,4.72647 a 15.353336,15.353336 0 0 0 -5.52369,3.88266 l -4.09702,-2.59362 -2.98727,4.36589 4.24375,2.68615 a 15.353336,15.353336 0 0 0 -1.09312,4.38492 15.353336,15.353336 0 0 0 -0.0396,2.12952 l -4.91831,1.08058 1.31928,5.1233 4.72688,-1.03871 a 15.353336,15.353336 0 0 0 3.88225,5.52365 l -2.59363,4.09702 4.36589,2.98727 2.68616,-4.24375 a 15.353336,15.353336 0 0 0 4.38491,1.09312 15.353336,15.353336 0 0 0 2.129526,0.0396 l 1.08099,4.91836 5.1229,-1.31932 -1.03872,-4.72689 a 15.353336,15.353336 0 0 0 5.52365,-3.88224 l 4.09702,2.59362 2.98731,-4.3663 -4.24375,-2.68615 a 15.353336,15.353336 0 0 0 1.09308,-4.38451 15.353336,15.353336 0 0 0 0.0396,-2.12953 l 4.91836,-1.08098 -1.31933,-5.1229 -4.72647,1.03875 a 15.353336,15.353336 0 0 0 -3.88265,-5.5237 l 2.59362,-4.09703 -4.36589,-2.98726 -2.68615,4.24375 a 15.353336,15.353336 0 0 0 -4.38492,-1.09313 15.353336,15.353336 0 0 0 -2.129976,-0.0392 z" id="vid-322239635" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Penda Dia et Ouleye Diallo, habitantes des villages de Gassemberi et Toubel Bali" data-fond="penda-dia" data-toggle="modal" data-target="#modal-322263889" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Penda Dia et Ouleye Diallo</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 188.98033,189.1619 -5.26104,-0.55335 -0.67776,4.79158 a 15.353336,15.353336 0 0 0 -6.53201,1.70849 l -2.9329,-3.86142 -4.32419,3.04732 3.03806,3.99939 a 15.353336,15.353336 0 0 0 -2.55602,3.72681 15.353336,15.353336 0 0 0 -0.78098,1.98154 l -4.98594,-0.70558 -0.55353,5.2614 4.79194,0.67794 a 15.353336,15.353336 0 0 0 1.70812,6.53183 l -3.86142,2.9329 3.04732,4.32419 3.99938,-3.03806 a 15.353336,15.353336 0 0 0 3.72682,2.55602 15.353336,15.353336 0 0 0 1.98154,0.78098 l -0.70521,4.98613 5.26103,0.55334 0.67794,-4.79194 a 15.353336,15.353336 0 0 0 6.53183,-1.70812 l 2.9329,3.86142 4.32438,-3.04769 -3.03806,-3.99938 a 15.353336,15.353336 0 0 0 2.55583,-3.72645 15.353336,15.353336 0 0 0 0.78099,-1.98154 l 4.98612,0.70522 0.55335,-5.26104 -4.79158,-0.67776 a 15.353336,15.353336 0 0 0 -1.70849,-6.53201 l 3.86142,-2.9329 -3.04732,-4.32419 -3.99938,3.03806 a 15.353336,15.353336 0 0 0 -3.72681,-2.55602 15.353336,15.353336 0 0 0 -1.9821,-0.78079 z" id="vid-322263889" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Patricia Brunel Maillet, Vice Présidente du Département de la Drôme en charge de l’Environnement et de la Santé" data-fond="patricia-brunel" data-toggle="modal" data-target="#modal-322261050" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Patricia Brunel Maillet</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.5291667;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 203.94438,159.65314 -4.45958,-0.8406 -0.91419,4.0495 a 13.170985,13.170985 0 0 0 -5.70427,1.00451 l -2.23794,-3.50642 -3.91008,2.30344 2.31821,3.63172 a 13.170985,13.170985 0 0 0 -2.44574,3.00792 13.170985,13.170985 0 0 0 -0.80616,1.63969 l -4.21375,-0.95156 -0.84078,4.45989 4.0498,0.91437 a 13.170985,13.170985 0 0 0 1.00421,5.70409 l -3.50642,2.23794 2.30344,3.91008 3.63172,-2.31821 a 13.170985,13.170985 0 0 0 3.00792,2.44574 13.170985,13.170985 0 0 0 1.63969,0.80616 l -0.95125,4.21392 4.45958,0.84061 0.91437,-4.0498 a 13.170985,13.170985 0 0 0 5.70409,-1.00421 l 2.23793,3.50642 3.91027,-2.30374 -2.31821,-3.63172 a 13.170985,13.170985 0 0 0 2.44556,-3.00762 13.170985,13.170985 0 0 0 0.80616,-1.63969 l 4.21392,0.95126 0.84061,-4.45959 -4.0495,-0.91419 a 13.170985,13.170985 0 0 0 -1.00451,-5.70427 l 3.50642,-2.23794 -2.30344,-3.91008 -3.63172,2.31821 a 13.170985,13.170985 0 0 0 -3.00792,-2.44574 13.170985,13.170985 0 0 0 -1.64018,-0.80604 z" id="vid-322261050" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Souleye Ba, Adjoint au maire, Commune d’Agnam Civol" data-fond="souleye-ba" data-toggle="modal" data-target="#modal-322275775" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Souleye Ba</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.52916676;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 180.5346,106.63537 -6.19588,2.42341 2.01305,5.74346 a 19.308852,19.308852 0 0 0 -6.30782,5.68438 l -5.50802,-2.61724 -3.06795,5.90335 5.70523,2.71053 a 19.308852,19.308852 0 0 0 -0.70029,5.64007 19.308852,19.308852 0 0 0 0.27327,2.66465 l -5.97663,2.09434 2.42341,6.19639 5.74398,-2.01307 a 19.308852,19.308852 0 0 0 5.68387,6.30783 l -2.61724,5.50803 5.90334,3.06795 2.71054,-5.70523 a 19.308852,19.308852 0 0 0 5.64008,0.70029 19.308852,19.308852 0 0 0 2.66465,-0.27327 l 2.09486,5.97661 6.19586,-2.4234 -2.01306,-5.74398 a 19.308852,19.308852 0 0 0 6.30783,-5.68386 l 5.50802,2.61724 3.06793,-5.90386 -5.70522,-2.71053 a 19.308852,19.308852 0 0 0 0.70029,-5.63956 19.308852,19.308852 0 0 0 -0.27326,-2.66465 l 5.97662,-2.09486 -2.42341,-6.19587 -5.74346,2.01305 a 19.308852,19.308852 0 0 0 -5.68437,-6.30781 l 2.61723,-5.50803 -5.90335,-3.06795 -2.71052,5.70523 a 19.308852,19.308852 0 0 0 -5.64007,-0.70029 19.308852,19.308852 0 0 0 -2.66516,0.2738 z" id="vid-322275775" class="chemin"/>
                </a>
                <a class="lien-portrait" data-nom="Mass Diallo, Ingénieur hydraulicien à ADOS" data-fond="mass-diallo" data-toggle="modal" data-target="#modal-322249874" data-backdrop="static" data-keyboard="false" xlink:href="#" onclick="modalMute()" onmouseover="afficheNom(this)" onmouseout="effaceNom()">
                    <title>Mass Diallo</title>
                    <path style="opacity:1;fill:#000000;fill-opacity:0;stroke:#bd4b34;stroke-width:0.52916676;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 252.66025,160.03761 -5.26104,-0.55335 -0.67776,4.79158 a 15.353336,15.353336 0 0 0 -6.53201,1.70849 l -2.9329,-3.86142 -4.32419,3.04732 3.03806,3.99939 a 15.353336,15.353336 0 0 0 -2.55602,3.72681 15.353336,15.353336 0 0 0 -0.78098,1.98154 l -4.98594,-0.70558 -0.55353,5.2614 4.79194,0.67794 a 15.353336,15.353336 0 0 0 1.70812,6.53183 l -3.86142,2.9329 3.04732,4.32419 3.99938,-3.03806 a 15.353336,15.353336 0 0 0 3.72682,2.55602 15.353336,15.353336 0 0 0 1.98154,0.78098 l -0.70521,4.98613 5.26103,0.55334 0.67794,-4.79194 a 15.353336,15.353336 0 0 0 6.53183,-1.70812 l 2.9329,3.86142 4.32438,-3.04769 -3.03806,-3.99938 a 15.353336,15.353336 0 0 0 2.55583,-3.72645 15.353336,15.353336 0 0 0 0.78099,-1.98154 l 4.98612,0.70522 0.55335,-5.26104 -4.79158,-0.67776 a 15.353336,15.353336 0 0 0 -1.70849,-6.53201 l 3.86142,-2.9329 -3.04732,-4.32419 -3.99938,3.03806 a 15.353336,15.353336 0 0 0 -3.72681,-2.55602 15.353336,15.353336 0 0 0 -1.9821,-0.78079 z" id="vid-322249874" class="chemin"/>
                </a>
                </g>
                </svg>
            </div>
            <!-- Portraits - FIN -->

            <!-- Ambiance - DEBUT -->
            <div class="svg-ambiance">
                <svg version="1.1" viewbox="0 0 1500 1000" preserveAspectRatio="none" class="svg-content-ambiance">
                <g>
                <a class="lien-portrait" data-nom="Cabine de pompage" data-fond="cabine-pompage" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-320947301" xlink:href="#" onclick="modalMute()" onmouseover="afficheNomAmbiance(this)" onmouseout="effaceNom()">
                    <title>Cabine de pompage</title>
                    <polygon class="rayon-svg-ambiance chemin" id="vid-320947301" points="1500,0 1500,1000 750,1000"/>
                </a>
                <a class="lien-portrait" data-nom="Le puits" data-fond="puits" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-320947454" xlink:href="#" onclick="modalMute()" onmouseover="afficheNomAmbiance(this)" onmouseout="effaceNom()">
                    <title>Le puits</title>
                    <polygon class="rayon-svg-ambiance chemin" id="vid-320947454" points="1500,0 750,1000 0,1000" />
                </a>
                <a class="lien-portrait" data-nom="Le maraîchage" data-fond="maraichage" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-320947107" xlink:href="#" onclick="modalMute()" onmouseover="afficheNomAmbiance(this)" onmouseout="effaceNom()">
                    <title>Le maraîchage</title>
                    <polygon class="rayon-svg-ambiance chemin" id="vid-320947107" points="1500,0 0,1000 0,500" />
                </a>
                </g>
                </svg>
            </div>
            <img id="fond-ambiances" src="img/ambiances-aQuoiCaSert.png" alt="fond ambiances" />
            <!-- Ambiance - FIN -->

            <div id="modalWrapper">
                <div class="modal fade" tabindex="-1" id="modal-322238033" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">À quoi ça sert&nbsp;?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div id="iframe-322238033"></div>
                            </div>
                            <div class="modal-footer">
                                <a data-dismiss="modal" title="Fermer" class="ferme-video" onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a>
                                <p id="nom-322238033"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322269832" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">À quoi ça sert&nbsp;?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div id="iframe-322269832"></div>
                            </div>
                            <div class="modal-footer">
                                <a data-dismiss="modal" title="Fermer" class="ferme-video" onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a>
                                <p id="nom-322269832"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322252061" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322252061"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322252061"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322235608" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322235608"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322235608"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322241550" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322241550"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322241550"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322239635" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322239635"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322239635"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322263889" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322263889"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322263889"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322261050" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322261050"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322261050"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322275775" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322275775"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322275775"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-322249874" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title">À quoi ça sert&nbsp;?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-322249874"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p id="nom-322249874"></p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-320947301" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title" id="nom-320947301"></h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-320947301"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p>&nbsp;</p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-320947454" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title" id="nom-320947454"></h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-320947454"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p>&nbsp;</p></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modal-320947107" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><h5 class="modal-title" id="nom-320947107"></h5><button type="button" class="close" data-dismiss="modal" aria-label="Fermer" onclick="modalUnmute()"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body"><div id="iframe-320947107"></div></div>
                            <div class="modal-footer"><a data-dismiss="modal" title="Fermer" class="ferme-video"  onclick="modalUnmute()"><img src="img/fleche-retour.png" alt="flèche retour"></a><p>&nbsp;</p></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="hover-video"></div>

            <footer>
                <?php include("./retour-webdoc.php"); ?>  <!-- intègre la flèche retour au webdoc -->
                <audio id="ambiance" autoplay muted loop>
                    <source src="audio/a-quoi-ca-sert.mp3" type="audio/mpeg">
                    <source src="audio/a-quoi-ca-sert.ogg" type="audio/ogg">
                    Votre navigateur ne peut pas lire la bande son. Veuillez essayer avec un autre navigateur web, Firefox par exemple.
                </audio>
                <?php include("./equalizer.php"); ?>  <!-- intègre l'equalizer -->
                <?php include("./credits.php"); ?>  <!-- intègre les crédits -->
            </footer>
        </div>
        <script src="js/script.js"></script>
        <script src="https://player.vimeo.com/api/player.js"></script>
        <script src="js/script-niveau3.js"></script>
    </body>
</html>
