/*
 * Activation du son de la vidéo au clic sur l'equalizer
 */

var playerNiveau1 = new Vimeo.Player('video-niveau1');

function unmuteVideo() {
    playerNiveau1.getVolume().then(function (volume) {
        if (volume === 0) {
            playerNiveau1.setVolume(1);
        } else {
            playerNiveau1.setVolume(0);
        }
    });
}
