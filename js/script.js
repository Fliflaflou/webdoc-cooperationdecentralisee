// Animation du menu

$(document).ready(function () {
    // Ouverture du menu
    $("#menu").click(function () {
        $(this).fadeOut();
        $("#sous-menu").slideDown(800);
        $("#entrees-menu").slideDown(800);
        $("#rouage-menu").animate({top: '60px'}, {queue: false, duration: 800, delay: 200});
    });

    // Fermeture du menu
    $("#sous-menu").click(function () {
        $(this).slideUp(800, function () {
            $("#menu").fadeIn(400);
        });
        $("#entrees-menu").slideUp(800);
        $("#rouage-menu").animate({top: '0px'}, {queue: false, duration: 800, delay: 200});

    });
});

// Fonction pour son ON/OFF

var eltAudio = document.getElementById("ambiance");

// mute et unmute quand on clique sur l'equalizer
function enableMute() {
    if (eltAudio.muted) {
        eltAudio.muted = false;
    } else {
        eltAudio.muted = true;
    }
}

// mute quand la modale s'ouvre + stop l'animation de l'equalizer
function modalMute() {
    eltAudio.muted = true;
    $(".barre").removeClass("eq__bar");
}

/* 
 * Loader de page - le défilement commence quand la page est chargée
 */

function showPage() {
    $(document).ready(function () {
        $("#loader").css("display", "none");
        $("#leContenu").css("display", "block");
        $("#webdocContenu").css("display", "flex");

        // texte qui défile sur la page d'accueil
        $(".defile").animate({top: '10px'}, 20000, 'linear', function () {
            $("#entrer").fadeIn(500);
        });
        $("#down").click(function () {
            $(".defile").stop();
            $(".defile").animate({top: '10px'}, 1000, 'linear', function () {
                $("#entrer").fadeIn(500);
            });
        });
        $("#up").click(function () {
            $(".defile").stop();
            $(".defile").animate({top: '300px'}, 1000, 'linear');
            $(".defile").animate({top: '10px'}, 20000, 'linear');
        });
        
        // equalizer - anime et stop au clic
        $(".tip-son").fadeIn(3000); /* fait apparaître le tooltip au chargement de la page */
        setTimeout(function () {
            $(".tip-son").fadeOut(1000);
        }, 7000);
        $("#equalizer").click(function () {
            $(".barre").toggleClass("eq__bar");
            $(".tip-son").fadeOut();
        });
    });
}