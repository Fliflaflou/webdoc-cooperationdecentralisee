// Arret de la rotation du rouage central au hover
$(document).ready(function () {
    $(".chemin").mouseenter(function () {
        $("#img-rouage-centre").removeClass("rotation");
    });
    $(".chemin").mouseleave(function () {
        $("#img-rouage-centre").addClass("rotation");
    });
});

// Player Vimeo
var videoId = []; // tableau qui reprend l'id de toutes les videos de la page (identifiées avec la class "chemin")
var videoElts = new Array(); // variable qui renvoie le numéro de la vidéo (id sans le 'vid-')
var videoIframe = new Array();
var videoOptions = new Array();
var playerElts = new Array(); // tableau pour stocker les players Vimeo
var cheminElts = document.getElementsByClassName("chemin");

for (var i = 0; i < cheminElts.length; i++) {
    videoId.push(cheminElts[i].getAttributeNode("id").value);
    videoElts[i] = videoId[i].substring(4, videoId[i].length); // supprime le 'vid-' de l'id des chemins
    videoIframe[i] = "iframe-" + videoElts[i];
    videoOptions[i] = {
        id: 0,
        height: 400,
        frameborder: 0
    };
    videoOptions[i].id = videoElts[i];
    playerElts[i] = 0;
}

var videoNom = new Array(); // tableau pour stocker les noms de vidéo (dans l'attribut data-nom du html)
var lienElts = document.getElementsByClassName("lien-portrait");
for (var i = 0; i < lienElts.length; i++) {
    videoNom.push(lienElts[i].getAttributeNode("data-nom").value);
}

//Création du player Vimeo et affichage du nom dans la modal
for (var i = 0; i < lienElts.length; i++) {
    playerElts[i] = new Vimeo.Player(videoIframe[i], videoOptions[i]);
    $("#nom-" + videoElts[i]).append(videoNom[i]);
}

//Affichage du nom du portrait au hover
function afficheNom(obj) {
    $("body").addClass(obj.getAttributeNode("data-fond").value);
    $("#hover-video").append("<p class=\"hover-titre\"><span><i>" + obj.getAttributeNode("data-nom").value + "</i></span></p>");
}

//Affichage du nom de l'ambiance au hover
function afficheNomAmbiance(obj) {
    $("body").addClass(obj.getAttributeNode("data-fond").value);
    $("#hover-video").append("<p class=\"hover-titre-ambiance\"><span><i>" + obj.getAttributeNode("data-nom").value + "</i></span></p>");
}

//Suppression du nom au mouseleave
function effaceNom() {
    $("body").removeClass();
    $("#hover-video").empty();
}

//Mise en pause de la video - pas de reprise automatique du son d'ambiance quand on ferme la modal 
function modalUnmute() {
    for (var i = 0; i < cheminElts.length; i++) {
        playerElts[i].pause().then(function () {
        });
    }
}
