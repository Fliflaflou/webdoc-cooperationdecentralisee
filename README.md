# Webdoc-cooperationdecentralisee

Webdocumentaire Coopération Décentralisée sur un parternariat d'entraide autour de la problématique de l'accés à l'eau

# Structure du webdoc

## 3 niveaux de navigation
* Niveau 1 = page d'accueil index.php
* Niveau 2 = page de navigation dans les 7 thèmatiques + les acteurs + le sujet
* Niveau 3 = 7 thèmes, avec portraits et pastilles sonores

## Bootstrap

Utilisé principalement pour les modales :

* Crédits
* Vidéos du niveau 3

## Vimeo

Pour le stockage des vidéos

API Vimeo qui permet d'interagir avec le player : 

* pour la mise en route du son sur le niveau 1 (index.php) --> dans le fichier script-niveau1.js
* pour la mise en pause de la vidéo lorsque la modale est fermée en cours de lecture --> dans le fichier script-niveau3.js

L'API est appelée via un script en bas de page

## Audio

Les fichiers audio sont stockés en mp3 et ogg dans le dossier "audio"

Ils sont appelés dans le page à l'aide de la balise audio

L'activation du son se fait grâce à l'equalizer en bas de page, qui s'anime lorsque le son est en marche. L'animation est stoppée quand le son est arrêté. Le son s'arrête automatique lors de l'ouverture d'une modale --> voir fichier script.js avec les actions sur le son

Par défaut, le son est inactif à l'ouverture de la page (autoplay plus autorisé par les navigateurs récents tant qu'il n'y a pas eu d'interaction de la part du visiteur). Une infobulle s'affiche pour indiquer au visiteur que du son est disponible, et où cliquer pour activer le son.

## Page niveau 2 "Le webdoc"

Les thématiques sont cachées par défaut. Un arrière-plan en svg défini 7 zones triangulaires : chaque zone s'affiche au hover (onmouseenter), et disparaît quand la souris quitte la zone (onmouseleave) --> voir le fichier script-niveau2.js

L'orientation du texte dans les triangles est faites grâce au css - adaptation avec des mediaqueries en fonction du rapport hauteur / largeur du navigateur.

## Pages niveau 3

### Portraits

Les portraits dans les rouages sont composés : 

* d'une image de fond avec les portraits
* d'un fichier svg qui définit le contour des rouages, et permet un clic précis sur chaque zone

Au **hover** sur un des portraits :

* le fond de la page est modifié avec l'affichage en arrière-plan d'une photo pleine page du portrait choisi
* le nom et la fonction de la personne s'affichent dans le quart en bas à droite de la page, avec un picto de rouage devant
* le rouage animé arrête de tourner

Au **clic** sur un des portraits : 

* une modale s'ouvre avec la vidéo correspondante
* si le son d'ambiance était activé, il s'arrête


### Capsules son

Les capsules son sont composés : 

* d'une image de fond avec les illustrations des capsules
* d'un fichier svg qui définit le contour des capsules (triangles), et permet un clic précis sur chaque zone

Au **hover** sur une des capsules :

* le fond de la page est modifié avec l'affichage en arrière-plan d'une photo pleine page de la capsule choisie
* le titre de la capsule s'affiche dans le quart en bas à droite de la page, avec un picto "play" devant
* le rouage animé arrête de tourner

Au **clic** sur une des capsules : 

* une modale s'ouvre avec la vidéo correspondante
* si le son d'ambiance était activé, il s'arrête

# Git
## Rappels sur l'utilisation de git 

### Pour cloner le dépot : 
$git clone nomdudépot *copier/coller possible depuis l'interface du site*

### Pour se mettre à jour : 
$git pull

### Pour indexer une modification :
$git add lenomdufichier 
ou
$git add --all
ou
$git add -A
*-A pour --all*

*Git suit les fichiers, pas les dossiers. Du coup un "git add" sur un dossier ne donnera pas de résultat(d'où ma superbe image dans le dossier image)*

### Pour vérifier ce qui est indexé : 
$git status
*"Status" donne des informations sur l'état de ta branch, ce qui sera compris dans le prochain commit et ce qui est compris dans le commit actuel (si tu la lances après avoir commit). Normalement il y a un affichage en couleur (rouge = pas add vert = add ) et des petits + et -*

### Pour proposer un commit :
$git commit -m "Petitcommentairechouettequivabien" 
*Je sais qu'il existe une commande pour commit et add --all, mais je ne la retrouve pas...*
*Pour faire en une ligne add + commit : l'option -a demande à Git de mettre à jour les fichiers déjà existants dans son index. (merci openclassrooms :)*
$git commit -a -m "Petitcommentairechouettequivabien"

### Pour push : 
$git push


## Problèmes possibles 

### Identifiant non/mal-configuré : 
$git config --global user.mail "tonmail@blabla.bla" 
/!\ le message d'erreur de git te propose user.**email**, le **e** est de trop. il faut ecrire user.**mail**

ou par id : 
$git config --global user.name "Fliflaflou"

Pour être sûr de la modification : 
$git config --local -l

### Décalage de versions : 
Ça arrive quand on oublie de se mettre à jour avant de faire des modifications

#### Méthode soft : 
J'en cherche une :)

#### Méthode efficace : 
$git merge
ou
$git merge master
*Normalement ça fusionne avec, dans l'exemple, branch master. À vérifier !*
